#!/usr/bin/env python3

# WARNING
# i *really, really, really* despise this code,
# but it does exactly what i want it to do

from matplotlib.patches import PathPatch
from matplotlib.offsetbox import OffsetImage, AnnotationBbox
import json
import matplotlib.pyplot as plt
from cycler import cycler
import numpy as np
from datetime import datetime, date, timedelta
from skimage.io import imread
from urllib.error import HTTPError
import requests
import io
from PIL import Image
from itertools import chain

fig, ax_bar = plt.subplots()
scale = 0.5
fig.set_figwidth(22 * scale)
fig.set_figheight(15 * scale)

ax_pie = ax_bar.inset_axes([0.0, 0.4, 0.5, 0.5])
ax_pie.set_title("Time as King Of The H")

ax_bar.set_xlabel('Hour')
ax_bar.set_ylabel('Amount of messages')

with open('message_data.json') as f:
    data = json.load(f)

date = datetime.utcfromtimestamp(data["messages"][0]["created_at"]).date()
fig.suptitle(f'h Discord Activity ({date} - Europe/Berlin Time)')

users_msg_amount = {}
for user in data["users"]:
    msg_amount = sum(1 for e in filter(lambda x: x["author"] == user["id"],
                                       data["messages"]))

    users_msg_amount[user["id"]] = msg_amount

hours = list(range(0, 24))
ax_bar.set_xticks(hours)

# https://matplotlib.org/stable/tutorials/colors/colormaps.html
colors = plt.cm.get_cmap('tab20')(np.linspace(0, 1, 20))
ax_bar.set_prop_cycle(cycler('color', colors))
ax_pie.set_prop_cycle(cycler('color', colors))

koth_dt = datetime.combine(date, datetime.min.time())
koth = data["start_koth"]["id"]
users_koth_time = {}


def koth_time_add(koth, koth_dt_diff):
    print(f"KOTH {koth} for {koth_dt_diff}")
    if users_koth_time.get(koth) is None:
        users_koth_time[koth] = koth_dt_diff
    else:
        users_koth_time[koth] += koth_dt_diff


for message in reversed(data["messages"]):
    old_koth_dt = koth_dt
    koth_dt = datetime.utcfromtimestamp(message["created_at"])

    new_koth = message["author"]
    koth_time_add(koth, koth_dt - old_koth_dt)
    koth = new_koth
target_koth_dt = datetime.combine(
    date, datetime.min.time()) + timedelta(days=1)
koth_time_add(koth, target_koth_dt - koth_dt)

users_koth_time_sorted = sorted(users_koth_time.items(), key=lambda x: x[1])
users_koth_time_shaked = []
for i in range(len(users_koth_time_sorted)):
    users_koth_time_shaked.append(users_koth_time_sorted.pop(
        0 if i % 2 == 0 else len(users_koth_time_sorted) - 1))

pie_data = []
pie_images = []
pie_labels = []
for (user_id, koth_time) in users_koth_time_shaked:
    user = next(chain(iter([user for user in data["users"]
                            if user["id"] == user_id]), [data["start_koth"]]))

    pie_data.append(koth_time.seconds)
    pie_images.append(user["avatar_url"])
    pie_labels.append(f'{user["name"]} ({koth_time})')

legend_data = []
users_iter = sorted(users_msg_amount.items(),
                    key=lambda x: x[1], reverse=True)
base_amount = [0 for e in hours]
prev_msg_amount = 0
prev_msg_amount_idx = 0
for idx, (user_id, msg_amount) in enumerate(users_iter):
    user = next(iter([user for user in data["users"]
                      if user["id"] == user_id]))

    msg_amount_idx = idx
    if prev_msg_amount == msg_amount:
        # didn't change, keep previous
        msg_amount_idx = prev_msg_amount_idx
    else:
        # changed, use new idx
        prev_msg_amount_idx = idx
    prev_msg_amount = msg_amount

    legend_data.append(
        f'{msg_amount_idx + 1}. {user["name"]} ({msg_amount:,} message(s))')

    user_hours = list(map(lambda x: datetime.utcfromtimestamp(x["created_at"]).hour,
                          filter(lambda x: x["author"] == user["id"], data["messages"])))

    amount = list(map(lambda hour: sum(1 for e in filter(
        lambda user_hour: user_hour == hour, user_hours)), hours))

    ax_bar.bar(hours, amount, bottom=base_amount)
    base_amount = list(map(lambda x: x[0] + x[1], zip(amount, base_amount)))

ax_bar.legend(legend_data, loc='upper right')

ax_bar.set_axisbelow(True)
ax_bar.yaxis.grid(color='gray', linestyle='dashed')

wedges, texts = ax_pie.pie(pie_data, labels=pie_labels, wedgeprops={
    'linewidth': 0.5, "edgecolor": "k", "fill": False, })
for i in range(len(wedges)):
    # modified from https://stackoverflow.com/a/44529673
    wedge = wedges[i]

    img = None
    url = pie_images[i]
    print("url:", url)
    try:
        if url is not None:
            params = (('size', '1024'),)
            response = requests.get(url, params=params)
            if response.status_code == 200:
                bytes_img = io.BytesIO(response.content)
                img = Image.open(bytes_img).resize((1024, 1024)).convert('RGB')
                print(f"downloaded")
    except HTTPError as e:
        print(e)

    if img is None:
        print("not available")
        img = Image.new('RGB', (1024, 1024), color='black')
    img.putalpha(int(255.0 * 0.7))

    path = wedge.get_path()
    patch = PathPatch(path, facecolor='none')
    ax_pie.add_patch(patch)
    imagebox = OffsetImage(img, zoom=0.2, clip_path=patch, zorder=-10)
    ab = AnnotationBbox(imagebox, (0.0, 0.0),
                        xycoords='data', pad=0, frameon=False)
    ax_pie.add_artist(ab)

    wedge.set_zorder(10)


fig.tight_layout()

fig.savefig('plot.png')
# fig.show()
