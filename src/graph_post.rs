use std::{cmp::Ordering, iter};

use chrono::{Date, Local, Offset};
use color_eyre::eyre::{bail, Result};
use serde::{Deserialize, Serialize};
use serenity::{futures::StreamExt, model::prelude::User, CacheAndHttp};
use tokio::{fs::File, io::AsyncWriteExt, process::Command};

#[derive(Debug, Serialize, Deserialize)]
struct JsonMessage {
    author: u64,
    created_at: i64,
}

#[derive(Debug, Serialize, Deserialize)]
struct JsonUser {
    id: u64,
    name: String,
    discriminator: u16,
    avatar_url: Option<String>,
}

impl JsonUser {
    async fn from_serenity(ctx: &CacheAndHttp, msg_author: User) -> JsonUser {
        let avatar_url = super::H_GUILD
            .member(&ctx, msg_author.id)
            .await
            .ok()
            .map(|member| member.face());

        JsonUser {
            avatar_url,
            id: msg_author.id.0,
            name: msg_author.name,
            discriminator: msg_author.discriminator,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Json {
    start_koth: JsonUser,
    messages: Vec<JsonMessage>,
    users: Vec<JsonUser>,
}

pub async fn run(ctx: &CacheAndHttp, shown_date: Date<Local>) -> Result<()> {
    let mut start_koth = None;
    let mut json_messages = Vec::new();
    let mut json_users = Vec::<JsonUser>::new();

    let generate_plot = async {
        log::info!("Generating plot for {}", shown_date);
        let mut messages = super::H_CHANNEL.messages_iter(&ctx.http).boxed();
        while let Some(message_result) = messages.next().await {
            match message_result {
                Ok(msg) => {
                    if msg.author.bot {
                        continue;
                    }

                    let msg_time = msg.timestamp.with_timezone(&shown_date.timezone());
                    match msg_time.date().cmp(&shown_date) {
                        Ordering::Greater => continue,
                        Ordering::Less => {
                            start_koth = Some(JsonUser::from_serenity(ctx, msg.author).await);
                            break;
                        }
                        Ordering::Equal => (),
                    }

                    json_messages.push(JsonMessage {
                        author: msg.author.id.0,
                        created_at: (msg_time + shown_date.offset().fix()).timestamp(),
                    });

                    if !json_users.iter().any(|user| user.id == msg.author.id.0) {
                        json_users.push(JsonUser::from_serenity(ctx, msg.author).await);
                    }
                }
                Err(err) => {
                    println!("Failed to fetch a message: {}", err);
                }
            }
        }

        let json_vec = serde_json::to_vec_pretty(&Json {
            start_koth: start_koth.unwrap(),
            messages: json_messages,
            users: json_users,
        })?;

        let mut output_file = File::create("message_data.json").await?;
        output_file.write_all(&json_vec).await?;

        let plot_data = Command::new("python3").arg("plot_data.py").output().await?;
        if !plot_data.status.success() {
            let stderr_str = String::from_utf8_lossy(&plot_data.stderr);
            bail!("Failed to plot data: {:?}", stderr_str);
        }

        Ok(())
    };

    let unpin_oldest = async {
        let pins = super::H_CHANNEL.pins(&ctx.http).await?;
        if pins.len() == 50 {
            pins.last().unwrap().unpin(ctx).await?;
        }

        Ok(())
    };

    tokio::try_join!(generate_plot, unpin_oldest)?;

    let newest_graph = super::H_CHANNEL
        .send_files(&ctx.http, iter::once("plot.png"), |m| m)
        .await?;
    newest_graph.pin(&ctx).await?;

    Ok(())
}
