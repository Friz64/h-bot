mod graph_post;

use chrono::Local;
use color_eyre::eyre::{Result, WrapErr};
use log::LevelFilter;
use serenity::{
    client::{bridge::gateway::GatewayIntents, Context, EventHandler},
    model::channel::Message,
    model::{
        channel::{MessageType, Reaction},
        event::MessageUpdateEvent,
        guild::Member,
        id::ChannelId,
        id::EmojiId,
        id::RoleId,
        id::{GuildId, MessageId, UserId},
        misc::EmojiIdentifier,
        prelude::Activity,
        prelude::Ready,
    },
    Client,
};
use std::{env, time::Duration};
use tokio::{sync::Mutex, time};

const DISCORD_TOKEN_KEY: &str = "DISCORD_TOKEN";
const H_GUILD: GuildId = GuildId(549291528179154944);
const H_CHANNEL: ChannelId = ChannelId(768146963412942878);
const KOTH_ROLE: RoleId = RoleId(768509620238024744);
const H_EMOJI: &str = "🇭";

const VALID_EMOJIS: &[&str] = &[
    H_EMOJI,
    "<a:ha:768161472097681449>",
    "<:h9:768162389136506900>",
    "<:h8:768162389027192852>",
    "<:h7:768162388729135125>",
    "<:h6:768162389018673162>",
    "<:h5:768162389056421918>",
    "<:h4:768162388611825716>",
    "<:h3:768162389040300112>",
    "<:h2:768162389036105798>",
    "<:h14:768162388783530047>",
    "<:h13:768162389036105780>",
    "<:h12:768162389165080606>",
    "<:h11:768162389136244746>",
    "<:h10:768162389064417300>",
    "<:h1:768162389023260672>",
    "<:crooked_h:769672473011683398>",
];

const VALID_MESSAGES: &[&str] = &[
    "h",
    "<a:ha:768161472097681449>",
    "<:h9:768162389136506900>",
    "<:h8:768162389027192852>",
    "<:h7:768162388729135125>",
    "<:h6:768162389018673162>",
    "<:h5:768162389056421918>",
    "<:h4:768162388611825716>",
    "<:h3:768162389040300112>",
    "<:h2:768162389036105798>",
    "<:h14:768162388783530047>",
    "<:h13:768162389036105780>",
    "<:h12:768162389165080606>",
    "<:h11:768162389136244746>",
    "<:h10:768162389064417300>",
    "<:h1:768162389023260672>",
    "<:crooked_h:769672473011683398>",
];

struct DiscordHandler {
    current_koth: Mutex<UserId>,
}

impl DiscordHandler {
    async fn update_koth(&self, ctx: &Context) {
        let mut current_koth = self.current_koth.lock().await;
        match H_CHANNEL.messages(ctx, |b| b.limit(1)).await {
            Ok(msgs) => match msgs.into_iter().next() {
                Some(latest_message) => {
                    if latest_message.author.bot {
                        return;
                    }

                    let new_koth = latest_message.author.id;
                    let last_koth = {
                        let last_koth = *current_koth;
                        *current_koth = new_koth;
                        last_koth
                    };

                    if new_koth != last_koth {
                        let remove_old = async {
                            match H_GUILD.member(&ctx, last_koth).await {
                                Ok(mut last) => {
                                    if let Err(err) = last.remove_role(&ctx, KOTH_ROLE).await {
                                        log::error!("Failed to remove last KOTH role: {}", err);
                                    }
                                }
                                Err(err) => {
                                    log::error!("Failed to get last KOTH member: {}", err);
                                }
                            }
                        };

                        let add_new = async {
                            match H_GUILD.member(&ctx, new_koth).await {
                                Ok(mut new) => {
                                    if let Err(err) = new.add_role(&ctx, KOTH_ROLE).await {
                                        log::error!("Failed to add new KOTH role: {}", err);
                                    }
                                }
                                Err(err) => {
                                    log::error!("Failed to get new KOTH member: {}", err);
                                }
                            }
                        };

                        tokio::join!(remove_old, add_new);
                    }
                }
                None => {
                    log::error!("No latest message");
                }
            },
            Err(err) => {
                log::error!("Failed to get latest message: {}", err);
            }
        }
    }
}

#[serenity::async_trait]
impl EventHandler for DiscordHandler {
    async fn ready(&self, ctx: Context, ready: Ready) {
        log::info!("Connected as {:?}", ready.user.name);
        ctx.set_activity(Activity::listening("h")).await;
    }

    async fn message(&self, ctx: Context, msg: Message) {
        if !msg.author.bot || msg.kind == MessageType::PinsAdd {
            if VALID_MESSAGES.contains(&msg.content.as_str()) && {
                msg.author.id != *self.current_koth.lock().await
            } {
                let crooked_h = EmojiIdentifier {
                    animated: false,
                    id: EmojiId(769672473011683398),
                    name: "crooked_h".into(),
                };

                if let Err(err) = msg.react(&ctx, crooked_h).await {
                    log::error!("Failed to react with H emoji: {}", err);
                }
            } else {
                if let Err(err) = msg.delete(&ctx).await {
                    log::error!("Failed to delete invalid message: {}", err);
                }
            }
        }

        self.update_koth(&ctx).await;
    }

    async fn message_delete(
        &self,
        ctx: Context,
        _channel_id: ChannelId,
        _deleted_message_id: MessageId,
        _guild_id: Option<GuildId>,
    ) {
        self.update_koth(&ctx).await;
    }

    async fn reaction_add(&self, ctx: Context, reaction: Reaction) {
        let str_repr = reaction.emoji.to_string();
        if !VALID_EMOJIS.contains(&str_repr.as_str()) {
            if let Err(err) = reaction.delete(&ctx).await {
                log::error!("Failed to delete non-h reaction: {}", err);
            }
        }
    }

    async fn guild_member_addition(&self, ctx: Context, _: GuildId, member: Member) {
        if let Err(err) = member.edit(&ctx, |edit| edit.nickname("h")).await {
            log::error!("Failed to rename new member: {}", err);
        }
    }

    async fn message_update(
        &self,
        ctx: Context,
        _old_if_available: Option<Message>,
        _new: Option<Message>,
        event: MessageUpdateEvent,
    ) {
        if event.edited_timestamp.is_some() {
            if let Err(err) = event.channel_id.delete_message(&ctx, event.id).await {
                log::error!("Failed to delete edited message: {}", err);
            }
        }
    }
}

#[tokio::main]
async fn main() -> Result<()> {
    color_eyre::install()?;
    pretty_env_logger::formatted_timed_builder()
        .filter(Some("h_bot"), LevelFilter::Info)
        .init();

    let token = env::var(DISCORD_TOKEN_KEY)
        .wrap_err_with(|| format!("Failed to get token from env var {:?}", DISCORD_TOKEN_KEY))?;

    let handler = DiscordHandler {
        current_koth: Mutex::new(UserId(0)),
    };

    let mut client = Client::builder(token)
        .event_handler(handler)
        .intents(
            GatewayIntents::GUILD_MEMBERS
                | GatewayIntents::GUILD_MESSAGES
                | GatewayIntents::GUILD_MESSAGE_REACTIONS,
        )
        .await
        .wrap_err("Failed to create client")?;

    let cache_and_http = client.cache_and_http.clone();
    tokio::spawn(async move {
        let mut prev_day = Local::today().pred();
        loop {
            let current_day = Local::today();
            if current_day != prev_day {
                if let Err(err) = graph_post::run(&cache_and_http, prev_day).await {
                    println!("Graph posting failed: {}", err);
                }
            }

            prev_day = current_day;
            time::sleep(Duration::from_secs(1)).await;
        }
    });

    client.start().await.wrap_err("Failed to start client")?;

    Ok(())
}
